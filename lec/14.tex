\section{(Quasi-)coherent sheaves}
Let $(X,\ox)$ be a ringed space.
\begin{defn}
  A \emph{sheaf of $\ox$-modules} is a sheaf $\shf$ on $X$ such that for all open subsets $U\sse X$ the abelian group $\shf(U)$ is an $\ox(U)$-module and for all inclusions of open subsets $V\sse U \sse X$ the restriction map $\shf(U)\to\shf(V)$ is an $\ox(U)$-linear map (here, we regard $\shf(V)$ as an $\ox(U)$-module via restriction of scalars along the restriction $\ox(U)\to\ox(V)$).
  A \emph{morphism of $\ox$-modules} is a moprhism of sheaves $\pphi\mc\shf\to\shg$ such that for all open subsets $U\sse X$ the map $\pphi_U\mc\shf(U)\to\shg(U)$ is $\ox(U)$-linear.
\end{defn}
\begin{rem}
  The category of $\ox$-modules is easily seen to be an abelian category: For that, we only need that the sheafification of a presheaf of $\ox$-modules is naturally a sheaf of $\ox$-modules.
\end{rem}
\begin{defn}
  Let $\shf$ and $\shg$ be sheaves of $\ox$-modules. Their \emph{tensor product} is the sheafification of the assignment
  \[
  U\mapsto \shf(U)\tensor_{\ox(U)}\shg(U).
  \]
\end{defn}

\begin{rem}\label{14:construction-along-morphism-on-same-space}
  Let $X$ be a topological space with sheaves of rings $\ox$ and $\ox'$ on it and let $\pphi\mc \ox\to\ox'$ be a morphism of sheaves of rings. Then we get functors
  \[
  \modc(X,\ox')\leftrightarrows\modc(X,\ox)
  \]
  given by
  \[
  \modc(X,\ox')\to \modc(X,\ox),~\shg\mapsto\shg\tensor_{\ox}\ox'
  \]
  which is a sheaf of $\ox'$-module (after sheafification) by using the $\ox'(U)$-module structure on $\shg(G)(U)\tensor_{\ox(U)}\ox'(U)$. The other functor is given by viewing $\shf(U)$ as an $\ox'$-module along the morphism $\ox(U)\to\ox'(U)$.
\end{rem}

\begin{defn}
  Let $\shf$ be an $\ox$-module. Then $\shf$ is called
  \begin{enumerate}
    \item \emph{free} if there is an index set $I$ such that $\shf\cong \ox^{\oplus I}$.
    \item \emph{locally free} if there exists and open cover $X=\bigcup U_i$ such that $\restrict{\shf}{U_i}$ is a free $\restrict{\ox}{U_i}$ module and the ranks $\rank \restrict{\shf}{U_i}$ are constant.
  \end{enumerate}
\end{defn}

\begin{construction}
\leavevmode
\begin{enumerate}
\item
  Let $f\mc(X,\ox)\to(Y,\oo_Y)$ be a morphism of ringed spaces, i.e. $f\mc X\to Y$ is a continous map and $f^\#\mc \oo_Y\to f_{\ast}\ox$ is a morphism of sheaves of rings. The functor
  \[
  f_{\ast}\mc \asheaf(X)\to\asheaf(Y)
  \]
  restricts to a functor
  \[
  f_{\ast}\mc \modc(X,\ox)\to\modc(Y,\oo_Y).
  \]
  Combining this with \cref{14:construction-along-morphism-on-same-space}, which gives a functor \[\modc(Y,f_{\ast}\ox)\to\modc(Y,\oo_Y),\] we get a functor
  \[
  f_{\ast}\mc\modc(X,\ox)\to\modc(Y,\oo_Y).
  \]
\item The map $f^\#\mc \oo_y\to f_{\ast}\ox$ has the adjoint $f^\flat\mc f^{-1}\oo_y\to\ox$, so we get a functor
\[
\modc(y,\oy)\to \modc(X,f^{-1}\oy)
\]
which combined with the extension $-\tensor_{f^{-1}\oy}\ox$ gives a functor
\[
f^{\ast}\mc \mod(Y,\oy)\to\modc(x,\ox)
\]
\end{enumerate}
\end{construction}
\begin{prop}
  The functors constructed above form an adjunction
  \[
  f_{\ast}\mc\modc(X,\ox)\leftrightarrows\modc(Y,\oy)\mcc f^{\ast}
  \]
\end{prop}

\begin{defn}
  Let $\shf$ be a sheaf of $\ox$-modules. We say
  \begin{enumerate}
    \item that $\shf$ is \emph{quasi-coherent} if for all $x\in X$ there exists an open set $U\sse X$ and an exact sequence of the form
    \[
    \oo_U^{\oplus I}\to \oo_U^{\oplus J}\to\restrict{\shf}{U}\to 0.
    \]
    \item that $\shf$ is \emph{coherent} if for all $x\in X$ there exists an open $U\sse X$ and a surjection
    \[
    \oo_U^{\oplus}\twoheadrightarrow\restrict{\shf}{U}\to 0.
    \]
  \end{enumerate}
\end{defn}
\begin{defn}
  Coherent sheaves of $\ox$-modules are in particular quasi-coherent.
\end{defn}

\begin{construction}
  Let $A$ be a ring, $M$ an $A$-module and $X=\spec A$. We define an $\ox$-module $\wtilde{M}$ as
  \[
  \wtilde{M}(U) =
  \lset s\mc U\to \coprod M_{\idp}\ssp s \text{ satisfies i) and ii)}
  \rset
  \]
  where the conditions are given by:
  \begin{enumerate}
  \item It holds that $s(\idp)\in M_{\idp}$.
  \item Locally, $s$ is of the form $m/a$ for a $m\in M$ and an $a\in A$.
  \end{enumerate}
  This is, intentionally, similar to the construction of the structure sheaf. In particular, it holds that for $x=\idp\sse A$ a prime ideal it holds that $\wtilde{M}_x = M_{\idp}$ and for an element $a\in A$ we have $\wtilde{M}(D(a))  = M_a$. This construction is functorial too, so we obtain a functor:
  \[
  \widetilde{-}\mc \amod\to\modc(X,\ox),~M\mapsto\wtilde{M}
  \]
\end{construction}
\begin{lem}
  The functor $\widetilde{-}$ constructed above is fully faithful and exact.
\end{lem}
\begin{proof}
    Assume that
    \[
    0\to M' \to M \to M''\to 0
    \]
    is exact sequence of $A$-modules. Since localization is an exxact functor, the sequence
    \[
    0\to M'_{\idp}\to M_{\idp}\to M''_{\idp}\to 0
    \]
    is exact for all prime ideals $\idp\sse A$. But this implies that for all points $x\in X=\spec A$ the sequence
    \[
    0\to \widetilde{M'}\to \widetilde{M}\to \widetilde{M''}\to 0
    \]
    is exact after talking stalks, since for $\idp = x\in X$ it holds that
    \[
    \left(\widetilde{M}\right)_x \cong \widetilde{M}_{\idp},
    \]
    and similarly for $M'$ and $M''$. For faithfulness, let $\pphi\mc M\to N$ be a morphism such that $\wtilde{\pphi} = 0$. Since on global sections, we have that $\wtilde{\pphi}_X = \pphi$, it follows immediatley that $\pphi = 0$. \par
    For fullness, let $g\mc \wtilde{M}\to \wtilde{N}$ be a morphism in $\modc(X,\ox)$. Let $\pphi\defined g_X$ be the morphism on global sections, then $\wtilde{\pphi} = g$: For all prime ideals $\idp\sse A$, the diagram
    \[
    \begin{tikzcd}
      M
      \ar{r}[above]{\wtilde{\pphi}_X-g_X}
      \ar{d}
      &
      N
      \ar{d}
      \\
      M_{\idp}
      \ar{r}[below]{\wtilde{\pphi}_{\idp}-g_{\idp}}
      &
      N_{\idp}
    \end{tikzcd}
    \]
    commutes, and the horizontal maps are zero. So $\wtilde{\pphi}$ agrees with $g$ on all stalks, and hence they are equal.
\end{proof}
\begin{prop}
  Let $X=\spec A$ be affine. Then the functor $M\mapsto \wtilde{M}$ defines an equivalence of categories between the categories of $A$-modules and the category of quasi-coherent sheaves on $X$.
\end{prop}
\begin{cor}
  The category $\qcoh(X)$ is an abelian category.
\end{cor}

\begin{prop}
  Let $X = \spec A$ be affine and let $\shf \in \qcoh(X)$ be a quasi-coherent $\ox$-module. Then $\hh^i(X,\shf) = 0$ for all $i>0$.
\end{prop}
\begin{proof}
  Let $M$ be an $A$-module. Taking an arbitrary (not necessarily finite!) generating set of $M$ (for example, $M$ itself), we get an exact sequence of the form.
  \[
  A^{\oplus I}\to A^{\oplus J}\twoheadrightarrow M \to 0
  \]
  Since by the above lemma the functor $\wtilde{(-)}$ is exact, the sequence
  \[
  \ox^{\oplus I}\to\ox^{\oplus J}\to \wtilde{M}\to 0
  \]
  is exact too, and hence $\wtilde{M}$ is indeed quasi-coherent. We now only need to show that $\wtilde{(-)}$ is essentially surjective, i.e. that we can find, for every quasi-coherent $\ox$-module $\shf$, an $A$-module $M$ such that $\wtilde{M} \cong \shf$ holds. For that, we proceed in two steps:
  \begin{enumerate}
    \item Assume there exists an exact global free presentation of $\shf$ of the form
    \begin{equation}\label{14:globalres}\tag{$\ast$}
    \ox^{\oplus I}\to\ox^{\oplus J}\to \shf\to 0.
    \end{equation}
    Since $\Gamma(X,-)$ is not right-exact, in the commutative diagram of global sections
    \[
    \begin{tikzcd}
      \ox(X)^{\bigoplus I}
      \ar{r}
      \ar[equal]{d}
      &
      \ox(X)^{\bigoplus J}
      \ar{r}
      \ar[equal]{d}
      &
      \shf(X)
      \\
      A^{\bigoplus I}
      \ar{r}
      &
      A^{\bigoplus J}
      \ar[twoheadrightarrow]{r}
      &
      \wtilde{M}
      \ar[hookrightarrow]{u}
    \end{tikzcd},
    \]
    the upper horizontal row need not be exact. After applying $\wtilde{(-)}$ to the lower row, we get the commutative diagram
    \[
    \begin{tikzcd}
      \wtilde{A^{\oplus I}}
      \ar{r}
      \ar{d}[left]{\cong}
      &
      \wtilde{A^{\oplus J}}
      \ar{d}[left]{\cong}
      \ar{r}
      &
      \wtilde{M}
      \ar{d}[left]{\cong}
      &
      \\
      \ox^{\ds I}
      \ar{r}[below]{\pphi}
      &
      \wtilde{A^{\ds J}}
      \ar{r}
      &
      \coker(\pphi)
      &
      0
    \end{tikzcd}
    \]
    where $\coker(\pphi) \cong \shf$, by \eqref{14:globalres}.
  \end{enumerate}
\end{proof}
\begin{cor}
  If
  \[
  0\to\shf\to\shg\to\shh\to 0
  \]
  is a short-exact sequence of $\ox$-modules with $\shf$ quasi-coherent, then the map $\shg(X)\to\shh(X)$ is surjective.
\end{cor}

\begin{cor}
  Let $X$ be an arbitrary scheme. Then the subcategory of quasi-coherent scheaves $\qcoh(X,\ox)\sse\modx(X,\ox)$ is closed under extensions, i.e. if
  \[
  0\to\shf\to\shg\to\shh\to 0
  \]
  is a short-exact sequence of $\ox$-modules with $\shf$ and $\shh$ quasi-coherent, then $\shg$ is quasi-coherent too.
\end{cor}
