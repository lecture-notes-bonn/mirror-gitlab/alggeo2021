\section{Sheaf Cohomology}

\begin{numtext}
  Let $X$ be a topological space. Then taking global sections defines a functor $\Gamma\mc \asheaf(X)\to\abc$, which is in general only left-exact. The vague idea of cohomology is to introduce for every sheaf $\shf$ a collection of abelian groups $\hh^i(\shf)_{i\geq 0}$ such that
  \begin{itemize}
    \item The assignement $\hh^i\mc \asheaf\to\abc,~\shf\mapsto\hh^i(\shf)$ is functorial for every $i\geq 0$;
    \item In degree zero, $\hh^0(\shf)$ coincides with the global sections of $\shf$, i.e. there is a natural isomorphism $\hh^0(\shf)\cong\Gamma$;
    \item If
    \[
    \xi\mc 0\to\shf_1\to\shf_2\to\shf_3\to 0
    \]
    is a short-exact sequence of sheaves, then for every $i\geq 0$ there is a map
    \[
    \delta^i_{\xi}\mc \hh^i(\shf_3)\to \hh^{i+1}(\shf_1);
    \]
    these maps fit into a long-exact sequence of the form
    \[
    \begin{tikzcd}[column sep = small]
      0\ar{r}&
      \Gamma(\shf_1)\ar{r}&
      \Gamma(\shf_2)\ar{r}
      \ar[draw=none]{d}[name=X, anchor=center]{}
      &
      \Gamma(\shf_3)\ar[rounded corners,
              to path={ -- ([xshift=2ex]\tikztostart.east)
                        |- (X.center) \tikztonodes
                        -| ([xshift=-2ex]\tikztotarget.west)
                        -- (\tikztotarget)}]{dll}[at end, above]{\delta^0}
      \\
      &
      \hh^1(\shf_1)\ar{r}&
      \hh^1(\shf_2)\ar{r}\ar[draw = none]{d}[name=Y, anchor = center]{}&
      \hh^1(\shf_3)\ar[rounded corners,
              to path={ -- ([xshift=2ex]\tikztostart.east)
                        |- (Y.center) \tikztonodes
                        -| ([xshift=-2ex]\tikztotarget.west)
                        -- (\tikztotarget)}]{dll}[at end, above]{\delta^1}
      \\
      &
      \hh^2(\shf_2)\ar{r}&
      \ldots
      &
    \end{tikzcd}
    \]
    \item The morphisms in the long-exact sequence are functorial in the following sense: If
    \[
    \begin{tikzcd}
      0
      \ar{r}
      &
      \shf_1
      \ar{r}
      \ar{d}[right]{\pphi_1}
      &
      \shf_2
      \ar{r}
      \ar{d}[right]{\pphi_2}
      &
      \shf_3
      \ar{r}
      \ar{d}[right]{\pphi_3}
      &
      0
      \\
      0
      &
      \shf_1'
      \ar{r}
      &
      \shf_2'
      \ar{r}
      &
      \shf_3'
      \ar{r}
      &
      0
    \end{tikzcd}
    \]
    is a commutative diagram of sheaves with short-exact rows $\xi$ and $\xi'$, then the diagram
    \[
    \begin{tikzcd}
    \hh^i(\shf_3)
    \ar{r}[above]{\delta^i_{\xi}}
    \ar{d}[left]{\hh^i(\pphi_3)}
    &
    \hh^{i+1}(\shf_3)
    \ar{d}[right]{\hh^{i+1}(\pphi_1)}
    \\
    \hh^i(\shf_3')
    \ar{r}[below]{\delta^i_{\xi'}}
    &
    \hh^{i+1}(\shf_1')
    \end{tikzcd}
    \]
    is commutative for every $i\geq 0$.
  \end{itemize}
\end{numtext}
We mimic the goal of this construction in the following definition:
\begin{defn}
  Let $\acat,\bcat$ be abelian categories. A \emph{$\delta$-functor} is a collection $\left(\hh^i\mc \acat\to\bcat\right)_{i\geq 0}$ of additive functors and a collection of morphisms $\delta^i_{\xi}$ for every short-exact sequence
  \[
  \xi\mc 0\to A_1\to A_2 \to A_3\to 0
  \]
  in $\acat$ such that the following conditions are satisfied:
  \begin{enumerate}
  \item
  For every such short-exact sequence $\xi$ the sequence
  \[
  \begin{tikzcd}[column sep = small]
    0\ar{r}&
    \hh^0(A_1)\ar{r}&
    \hh^0(A_2)\ar{r}
    \ar[draw=none]{d}[name=X, anchor=center]{}
    &
    \hh^0(\shf_3)\ar[rounded corners,
            to path={ -- ([xshift=2ex]\tikztostart.east)
                      |- (X.center) \tikztonodes
                      -| ([xshift=-2ex]\tikztotarget.west)
                      -- (\tikztotarget)}]{dll}[at end, above]{\delta^0}
    \\
    &
    \hh^1(A_1)\ar{r}&
    \hh^1(A_2)\ar{r}\ar[draw = none]{d}[name=Y, anchor = center]{}&
    \hh^1(A_3)\ar[rounded corners,
            to path={ -- ([xshift=2ex]\tikztostart.east)
                      |- (Y.center) \tikztonodes
                      -| ([xshift=-2ex]\tikztotarget.west)
                      -- (\tikztotarget)}]{dll}[at end, above]{\delta^1}
    \\
    &
    \hh^2(A_1)\ar{r}&
    \ldots
    &
  \end{tikzcd}
  \]
  is exact.
  \item
  For every commutative diagram of with short-exact rows $\xi$ and $\xi'$
  \[
  \begin{tikzcd}
    0
    \ar{r}
    &
    A_1
    \ar{r}
    \ar{d}[right]{\pphi_1}
    &
    A_2
    \ar{r}
    \ar{d}[right]{\pphi_2}
    &
    A_3
    \ar{r}
    \ar{d}[right]{\pphi_3}
    &
    0
    \\
    0
    &
    A_1'
    \ar{r}
    &
    A_2'
    \ar{r}
    &
    A_3'
    \ar{r}
    &
    0
  \end{tikzcd}
  \]
  the induced diagram
  \[
  \begin{tikzcd}
  \hh^i(A_3)
  \ar{r}[above]{\delta^i_{\xi}}
  \ar{d}[left]{\hh^i(\pphi_1)}
  &
  \hh^{i+1}(A_1)
  \ar{d}[right]{\hh^{i+1}(\pphi_1)}
  \\
  \hh^i(A_3')
  \ar{r}[below]{\delta^i_{\xi'}}
  &
  \hh^{i+1}(A_1')
  \end{tikzcd}
  \]
  is commutative for every $i\geq 0$.
  \end{enumerate}
\end{defn}

\begin{defn}
  A $\delta$-functor $(\hh^i,\delta^i)\mc\acat\to\bcat$ is \emph{universal} if for all other $\delta$-functors $\left(\wtilde{\hh}^i,\wtilde{\delta}^i\right)\mc \acat \to \bcat$ and for all natural transformations $\hh^0\to \wtilde{\hh}^0$ there are unique natural transformations $\hh^i \to \wtilde{\hh}^i$ that are compatible with the $\delta$'s in the sense that for every short-exact sequence
  \[
  \xi\mc 0\to A_1\to A_2\to A_3 \to 0
  \]
  the diagram
  \[
  \begin{tikzcd}
  \hh^i(A_3)
  \ar{r}[above]{\delta^i_{\xi}}
  &
  \hh^{i+1}(A_1)
  \ar{d}
  \\
  \wtilde{\hh}^i(A_3)
  \ar{r}[below]{\wtilde{\delta}^i_{\xi}}
  &
  \wtilde{\hh}^{i+1}(A_1)
  \end{tikzcd}
  \]
  is commutative.
\end{defn}
\begin{defn}
A functor $F\mc \acat\to\bcat$ is effa\c{c}eable if for every object $A$ of $\acat$ there exists a monomorphisms $\pphi\mc A\hookrightarrow A'$ such that $F(\pphi) = 0$.
\end{defn}
\begin{theorem}
  If $\left(\hh^i,\delta^i\right)\mc\acat\to\bcat$ is a $\delta$-functor such that all $\hh^i$ are effa\c{c}eable for $i>0$ then it is already a universal $\delta$-functor.
\end{theorem}
\begin{defn}
  An object $I$ of $\acat$ is \emph{injective} if for every monomorphism $A_1\hookrightarrow A_2$ and morphism $A_1\to A_2$ there is a (generally not unique) map $A_2 \to I$ such that the diagram
  \[
  \begin{tikzcd}
    A_1
    \ar[hookrightarrow]{r}
    \ar{d}
    &
    A_2
    \ar[dashed]{ld}[below right]{\exists}
    \\
    I
    &
  \end{tikzcd}
  \]
  commutes.
\end{defn}
\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item We say that \emph{$\acat$ has enough injective objects} if for every object $A$ there is a monomorphism $A\hookrightarrow I$ with $I$ an injective object.
    \item An \emph{injective resolution} of an object $A$ is a long-exact sequence of the form
    \[
    0\to A\to I_0\to I_1\to I_2\to \hdots
    \]
    such that all the $I_i$ are injective objects of $\acat$.
  \end{enumerate}
\end{defn}
\begin{construction}
  Let $F\mc \acat\to \bcat$ be a left-exact additive functor and assume that $\acat$ has enough injective objects. Choose an injective resolution
  \[
  \xi\mc 0\to A\to I_1\to I_2\to \ldots
  \]
  of $A$. Define
  \[
  \rh^i_{\xi}F\defined \hh^i\left(FI_0\to FI_1\to FI_2\to\ldots\right).
  \]
  We call the $\rh^i_{\xi}F$ the \emph{higher derived functors of $F$}. Note that a priori $\rh^i_{\xi}F(A)$ depends on the chosen injective resolution of $A$.
\end{construction}
\begin{prop}
  The above construction satisfies the following properties:
  \begin{enumerate}
  \item The functor $\rh^0F$ is naturally isomorphic to $F$.
  \item The objects $\rh^iFA$ are independent of the choice of injective resoluation.
  \item We can construct connecting homomorphisms.
  \item If $I$ is an injective object, then $\rh^iFI = 0$ for all $i>0$.
  \item The functors $\rh^iF$ are effa\c{c}eable for $i>0$.
  \end{enumerate}
\end{prop}
\begin{theorem}
  If $F\mc \acat\to \bcat$ is an additive left-exact functor and $\acat$ has enough injective objects, then the functors $(\rh^iF,\delta)$ constructed above define an universal $\delta$-functor.
\end{theorem}
\begin{defn}
  An object $A$ of $\acat$ is called \emph{$F$-acyclic} if $\rh^iFA = 0$ for all $i>0$.
\end{defn}
\begin{example}
  Any injective object is $F$-acyclic.
\end{example}

\begin{rem}
  In the construction of the derived functors, it is possible to replace the injective resolution of an object $A$ with an $F$-acylic resolution. Usually, $F$-acylic resolutions are easier to compute.
\end{rem}
