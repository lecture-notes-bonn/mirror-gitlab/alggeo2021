\subsection*{Open and closed subschemes}
\begin{defn}
  Let $(X,\ox)$ be a scheme. A scheme that is isomorphic to a scheme of the form $(U,\restrict{\ox}{U})$ is called an \emph{open subscheme of $X$}.
\end{defn}
\begin{rem}
  Open subschemes of affine schemes need not be affine again.
\end{rem}
\begin{defn}
  Let $(X,\ox)$ be a scheme. A \emph{closed subscheme} consists of the following data:
  \begin{enumerate}
    \item A closed subset $i\mc Z\hookrightarrow X$.
    \item A sheaf of rings $\oo_Z$ on $Z$ such that $(Z,\oo_Z)$ is a scheme.
    \item An isomorphism $\restrict{\ox}{\vartheta}\cong i_{\ast}\oo_Z$ for some sheaf of ideals $\vartheta\subseteq \ox$.
  \end{enumerate}
\end{defn}

\subsection*{First properties of schemes}
\begin{defn}[Topological Properties]
  Let $(X,\ox)$ be a scheme. We say
  \begin{enumerate}
    \item that it is \emph{connected} if $X$ is connected as a topological space: i.e. there is no decomposition $X = X_1\coprod X_2$ into disoint closed proper subsets $X_1,X_2\subsetneq X$.
    \item that is is \emph{irreducible} if $X$ is irreducible as a topological space: i.e. there is no decomposition $X = X_1\coprod X_2$ into closed proper subsets $X_1,X_2\subsetneq X$.
    This is the case if and only if for all $U\sse X$ open $\overline{U} = X$ if and only if every two non-empty open subsets $\emptyset\neq U,V\sse X$ have non-trivial intersection.
    \item that it is \emph{quasi-compact} if $X$ is quasi-compact as a topological space: i.e. for every open cover $X = \bigcup_{i\in I}U_i$ there exists a finite subset $I_0\sse I$ such that $X=\bigcup_{i\in I_0}U_i$, i.e. finitely many $U_i$ already cover $X$.
  \end{enumerate}
\end{defn}
\begin{rem}
  If $X$ is quasi-compact, then every closed subset is quasi-compact as well. The same is in general not true for open subsets.
\end{rem}
\begin{example}
  Let $(X,\ox)$ be an affine scheme. Then $(X,\ox)$ is quasi-compact.
\end{example}

\begin{defn}[Scheme-theoretic properties]
Let $(X,\ox)$ be a scheme. We say
\begin{enumerate}
  \item that it is \emph{locally Noetherian} if there exists an open cover by affine schemes $X=\bigcup U_i$ with $U_i = \spec(A_i)$ as schemes, such that each of the $A_i$ is a Noetherian ring.
  \item that it is \emph{Noetherian} if it is locally Noetherian and quasi-compact.
  \item that it is \emph{reduced} if for all open subsets $U\sse X$ the ring $\Gamma(U,\ox)$ is a reduced ring, i.e. has no non-zero nilpotent elements.
  \item that it is \emph{integral} if for all open subsets $U\sse X$ the ring $\Gamma(U,\ox)$ is an integral domain.
\end{enumerate}
\end{defn}
\begin{lem}
  Let $(X,\ox)$ be a scheme. Then $(X,\ox)$ is locally Noetherian if and only if for all open subschemes $U\sse X$ (i.e $(U,\restrict{\ox}{U})\cong (\spec A, \oo_{\spec A})$) the ring $A$ is Noetherian.
\end{lem}

\begin{cor}
  Let $A$ be a ring. Then the scheme $(\spec A,\oo_{\spec A})$ is Noetherian if and only if $A$ is a Noetherian ring.
\end{cor}
\begin{lem}
    Let $(X,\ox)$ be a scheme.  Then $X$ is reduced if and only if all stalks $\oxx$ are reduced rings.
\end{lem}
\begin{lem}
  Let $(X,\ox)$ be a scheme. Then $X$ is integral if and only if it is reduced and irreducible.
\end{lem}
\begin{cor}
  If $X$ is an integral scheme, then there exists a unique point $\eta \in X$ such that $\overline{\lset \eta\rset} = X$. This point is called the \emph{generic point of $X$}.
\end{cor}
\begin{mexample}
  For an integral affine scheme $X = \spec A$ with $A$ an integral domain the generic point is given by the prime ideal $\genby{0}$.
\end{mexample}
