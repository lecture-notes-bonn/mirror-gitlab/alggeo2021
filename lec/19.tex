\section{Invertible sheaves}
\begin{construction}
Let $(X,\ox)$ be a scheme. We define $\pic(X)$ as the set of invertible sheaves $\shl$ on $X$. This has a group structure, where the multiplication is given by the tensor product and the inverse is given by the dual sheaf. This group is called the \emph{Picard group of $X$}.
\end{construction}
\begin{rem}
  That a sheaf $\shl$ is invertible means concretely that there exists an open cover $X = \bigcup U_i$ and ``trivializations'' $\pphi_i\mc \restrict{\shl}{U_i}\cong \oo_{U_i}$. Now on the intersection, we get maps:
  \[
  \begin{tikzcd}
    \oo_{U_i\cap U_j}
    \ar{r}[above]{\cong}[below]{\pphi_j^{-1}}
    &
    \restrict{\shl}{U_i\cap U_j}
    \ar{r}[above]{\cong}[below]{\phi_i}
    &
    \oo_{U_i\cap U_j}
  \end{tikzcd}
  \]
  These maps satisfy $\pphi_{ii}=\id$, $\pphi_{ij}\circ\pphi_{ji} = \id$ as well as a \emph{cocycle-condition} in the following commutative diagram
  \[
  \begin{tikzcd}
    \oo_{U_i\cap U_k}
    \ar{rr}[above]{\pphi_{ik}}
    \ar{rd}[below left]{\pphi_{jk}}
    &&
    U_{ik}
    \\
    &
    U_{kj}
    \ar{ur}[below right]{\pphi_{jk}}
    &
  \end{tikzcd}
  \]
\end{rem}

\begin{example}
  Let $\shl\in \pic X$ be inverstible and $s_0,\ldots,s_n\in \hh^0(X,\shl)$ be global sections. Define
  \[
  X_i\defined X_{s_i}
  \defined
  \lset
  x\in X\ssp s_{i_x}\notin \idm_x\cdot \shl_x
  \rset
  \]
  If we set
  \[
  \shl(x)\defined \shl_x \tensor_{\oxx} \kappa(x)
  \]
  then elements $s\in \hh^0(X,\shl)$ define elements in the stalks $s_x\in \shl_x$ and thus elements $s(x)\in \shl(x)$, which enables us to write
  \[
  X_s =
  \lset
  x\in X\ssp s(x)\neq 0
  \rset
  \]
  We then have that the $s_0,\ldots,s_n$ generate $\shl$ if and only if the map
  \[
  \bigoplus_i \oo \xrightarrow{(s_i)}\shl\]
  is surjective
  if and only if the $X_i$ cover $X$.
  Assume now that this is the case. Then the restrictions $\restrict{s_i}{X_i}$ give trivializations $\pphi_i\mc \restrict{\shl}{X_i}\xrightarrow{\cong}\oo_{X_i}$. Then the cocycly condition for these $\pphi_i$ imply that for all $i,j$ it holds that
  \[
  \frac{s_j}{s_i} \in \Gamma\left(X_i\cap X_j,\shl\tensor \shl\dual\right)
  \]
 \end{example}


\begin{numtext}
  In the general situation, we also have maps
  \begin{align*}
  \pic X &\to \chh^1\left(X,\ox\dual\right)\cong \hh^1\left(X,\ox^{\ast}\right)\\
  \left(\shl,(\pphi_i)\right)&
  \mapsto
  \lset
  \pphi_{ij}\in \ch^1\left(\lset U_i\rset,\ox^{\ast}\right)\in  \chh^1\left(\lset U_i\rset, \oo^{\ast}\right)
  \rset
  \end{align*}

\end{numtext}

\begin{prop}
  This map is a well-defined isomorphism of groups.
\end{prop}
,\begin{rem}
  We have a map $\zz\to\pic \pp_A^n$, given by mapping to the twisting sheaf: $d\mapsto \oo(d)$.
  If $A=k$ is a field, we will later see that this yields an isomorphism $\pic \pp_k^n\cong \zz$.
\end{rem}

\begin{rem}
\leavevmode
\begin{enumerate}
  \item If $f\mc X\to Y$ is a morphism, we get an induced group homomorphism:
  \[
  f^{\ast}\mc
  \pic Y\to \pic X,
  \shl \mapsto
  f^{\ast}\shl\defined f^{-1}\shl \tensor_{f^{-1}\oy}\ox
  \]
\end{enumerate}
\end{rem}

\begin{prop}
  Let $X\to \spec A$ be a morphism of schemes, $\shl \in \pic X$ and $s_o,\ldots,s_n\in \hh^0(X,\shl)$ generating elements.
  Then there exists a factorization of the form
  \[
  \begin{tikzcd}
    X
    \ar{rr}[above]{f}
    \ar{rd}
    &
    &
    \pp_A^n
    \ar{ld}
    \\
    &
    \spec A
    &
  \end{tikzcd}
  \]
  such that $f^{\ast}\oo(1)\cong \shl$ and elements $x_i$ satisfying $s_i = f^{\ast}x_i$.
\end{prop}

\begin{rem}
  If the $s_0,\ldots,s_n$ do not generate $\shl$, then the above still defines a morphism
  \[
  \bigcup X_{s_i}\to \pp^n_A.
  \]
\end{rem}

\begin{defn}
  Let $X$ be a Noetherian scheme and $\shl\in\pic X$. We say that $\shl$ is \emph{ample on $X$} if for coherent modules $\shf \in \coh X$ the module $\shf\tensor \shl^n$ is globally generated for all $n\geq n_0$.
\end{defn}

\begin{example}
  Let $A$ be a Noetherian ring and $X  =\spec A$ affine. Then every invertible sheaf $\shl$ on $X$ is ample.
\end{example}

\begin{lem}
  Let $X$ be a Noetherian scheme, $\shl$ an invertible module on $X$. Then the followig are equivalent:
  \begin{enumerate}
    \item The sheaf $\shl$ is ample.
    \item For all $n>0$, $\shl^n$ is ample.
    \item There exists a $n>0$ such that $\shl^n$ is ample.
  \end{enumerate}
\end{lem}

\begin{prop}
Let $A$ be a Noetherian ring, $f\mc X\to \spec A$ be a morphism of finite type and $\shl\in \pic X$.
Then $\shl$ is ample if and only if there exists a factorization of the form
\[
\begin{tikzcd}
    X
    \ar[hookrightarrow]{rr}[above]{i}
    \ar{rd}
    &
    &
    \pp_A^n
    \ar{ld}
    \\
    &
    \spec A
    &
\end{tikzcd}
\]
such that $i$ is an open immersion satisfying $i^{\ast}\oo(1)\cong\shl^m$ for some $m>0$.
\end{prop}
