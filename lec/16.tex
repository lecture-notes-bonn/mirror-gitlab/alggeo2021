\begin{construction}
  Let $B = \bigoplus_{d\geq 0}B_d$ be a graded ring. As a set, we define:
  \[
  \proj B
    \defined
  \lset \idp \sse B\ssp
  \begin{array}{l}
    \idp\text{ is a homogenous prime ideal,}\\
    B_+\ssne \idc
  \end{array}
  \rset
  \]
  In order to get a \emph{Zariski topology on $\proj B$}, we define for an homogenous ideal $\ida\sse B$:
  \[
  V_+(\ida)
  \defined
  \lset \idp\in\proj B \ssp \ida\sse \idp
  \rset
  \]
  As in the affine case declaring subsets of the form $V_+(\ida)\sse \proj B$ to be the closed sets defines a topology on $\proj B$.
\end{construction}

\begin{prop}
  Let $B = \bigoplus_{d\geq 0}B_d$ be a graded ring, $a\in B_d$ an element, $d>0$. Set $B_{(a)}\defined \left(B_a\right)_0$. Then there exists a natural homeomorphism
  \[
  D_{+}(a)\cong \spec(B_{(a)}).
  \]
\end{prop}
\begin{construction}
  We now want to establish a meaningful structure sheaf $\oo$ on $\proj B$. For that, we define for an open subset $U\sse \proj B$:
  \[
  \oo(U)\defined
  \lset
  s\mc U \to \coprod_{\idp \in U}B_{(\idp)}\ssp
  \begin{array}{l}
    s(\idp)\in B_{(\idp)}\\
    \text{locally, it holds that} s = a/b\text{ with }a,b\in B_d
  \end{array}
  \rset
  \]
  This makes $\oo$ into a sheaf of rings on $\proj B$.
\end{construction}
\begin{prop}
  \leavevmode
  \begin{enumerate}
    \item For all $\idp\in \proj B$, it holds that $\oo_{\idp}\cong B_{(\idp)}$, so $(\proj B,\oo)$ is a locally ringed space.
    \item For all $a\in B_{d>0}$, it holds that $(D_{+}(a),\restrict{\oo}{D_{+}(a)})\cong (\spec(B_{(a)}),\oo)$.
  \end{enumerate}
\end{prop}

\begin{cor}
  Indeed, $(\proj B,\oo)$ is a scheme.
\end{cor}

\begin{defn}
  Let $A$ be a ring. We define $\pp_A^n\defined \proj A[x_0,\ldots,x_n]$ which we call the \emph{projective space over $A$}. For any scheme $X$, we define $\pp^n_X$ as the fibre product:
  \[
  \begin{tikzcd}
      \pp_X^n
      \ar{r}
      \ar{d}
      \ar[phantom]{dr}{\lrcorner}
      &
      \pp_{\zz}^n
      \ar{d}
      \\
      X
      \ar{r}
      &
      \spec \zz
  \end{tikzcd}
  \]
\end{defn}

\begin{construction}
    Let $B = \bigoplus_{d\geq 0}B_d$ be a graded ring, and $M = \bigoplus_{n\in \zz}M_n$ a graded $B$-module, i.e. $M$ is a $B$-module and $B_d\cdot M_n\sse M_{d+n}$. For an element $a\in B_d$ we define the \emph{homogenous localization} as
    \[
    M_{(a)}\defined \lset \frac{m}{a^n}\ssp m\in M_{nd}
    \rset ,
    \]
    which is naturally a $B_{(a)}$-module and a subset of $M_a$.\par
    In a similar vein, we define for an element $\idp \in \proj B$ first the multiplicative subset
    \[
    S\defined
    \lset b\in B\setminus \idp\text{ homogenous}\rset
    \]
    and then
    \[
    M_{(\idp)}\defined \lset \frac{m}{b}\ssp m\in M_n, b\in B_n
    \rset
    \]
    which is a subset of
    \[
    S^{-1}M = \bigoplus \left(S^{-1}M\right)_d.
    \]
  \end{construction}

  \begin{construction}
    Let $M$ be a graded $B$-module. We then define a sheaf of modules on $(\proj B,\oo)$ by setting
    \[
    \wtilde{M}(U)
    \defined
    \lset
    s\mc U\to \coprod_{\idp \in U}\ssp
    \begin{array}{l}
      s(\idp)\in M_{(\idp)}\text{ and}
      \\
      \text{locally }s=\frac{m}{b}\text{ with }b\in B_d,b\notin\idp,m\in M_a
    \end{array}
    \rset
    \]
  \end{construction}

\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item If $\idp\in \proj B$ then $\left(\wtilde{M}\right)_{\idp}\cong M_{(\idp)}$.
    \item If $a\in B_d$ then $\restrict{\wtilde{M}}{D_{+}(a)}\cong \widetilde{M_{(a)}}$ via the isomorphism $D_+(a)\cong \spec B_{(a)}$.
    \item It holds that $\wtilde{M}\in \qcoh(\proj B)$.
    \item If $B$ is Noetherian and $M$ is a finite $B$-module then $\wtilde{M}\in \coh(\proj B)$.
  \end{enumerate}
\end{rem}
\begin{construction}
Let $M$ be a graded $B$-module and $d\in \zz$. Then the \emph{shifted} graded $B$-module $M(d)$ is $M$ with the grading
\[
M(d) = \bigoplus_{n\in \zz}M(d)_n\text{ with }M(d)_n\defined M_{n+d}.
\]
\end{construction}
\begin{defn}
  Let $B$ be a graded ring. Then we define the \emph{$d$-th twisting sheaf $\oo(d)$ over $B$} as $\oo(d)\defined \widetilde{B(d)}$, which is a quasi-coherent sheaf of $\proj B$-modules.
\end{defn}

\begin{numtext}
In what follows, we will often need that a graded ring $B$ is generated as a $B_0$-algebra by $B_1$. We will denote this assumption by $(\#)$.
\end{numtext}

\begin{lem}
  Let $B$ be a graded ring and assume $(\#)$.
  \begin{enumerate}
    \item The $d$-th twisting sheaf $\oo(d)$ is \emph{invertible}, i.e. locally free of rank $1$.
    \item It holds that
    \[
    \oo(d_1)\tensor_{\oo}\oo(d_1)\cong \oo(d_2)\cong \oo(d_1+d_2)
    \]
    \item If $M$ is a graded $B$-module, then
    \[
    \wtilde{M}\tensor_{\oo}\oo(d)\cong \widetilde{M(d)}.
    \]
  \end{enumerate}
\end{lem}

\begin{rem}
  In contrast to the affine case, the assignement
  \[
  B\grmodc\to \qcoh(\proj B)
  \]
  is in general not injective. If $M$ is a graded $B$-module and there is a lower bound $N'$ such that $M_N=0$ for all $N>N'$, then $\wtilde{M} = 0$.
\end{rem}

\begin{construction}
  Let $B$ be a graded ring that satisfies $(\#)$ and let $\shf \in \qcoh(\proj B)$ be quasi-coherent. Then define the \emph{$d$-th twist of $\shf$} as
  \[
  \shf(d)\defined \shf\tensor_{\oo}\oo_x(d).
  \]
  We now generalize the global sections functor $\Gamma$ to modules over $\proj B$, by setting
  \[
  \Gamma_{\ast}\defined \qcoh(\proj B)\to B\grmodc,~\shf\mapsto \bigoplus_{d\in \zz}\gamma(\proj B,\shf(d)).
  \]
\end{construction}

\begin{prop}
  Let $B$ be a graded ring that satisfies $(\#)$ and let $\shf \in \qcoh(\proj B)$.
  Then there is a naturally isomorphism
  \[
  \widetilde{\Gamma_{\ast}(\shf)}\cong \shf.
  \]
  In particular, the functors $\wtilde{(-)}$ and $\Gamma_{\ast}$ are adjoint.
\end{prop}
