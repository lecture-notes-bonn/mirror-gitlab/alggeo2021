\begin{construction}
\leavevmode
\begin{enumerate}
\item
  Let $\pphi\mc \shf\to\shg$ be a morphism in $\psc{\ccat}$, where $\ccat$ is an abelian category. Then the \emph{presheaf-kernel}, \emph{presheaf-cokernel} and \emph{presheaf-image} are the respective assignments:
  \[
  \ker^\pre(\pphi)(U)\defined \ker \shf(U)\xrightarrow{\pphi_U}\shg(U),~\coker^\pre(\pphi)(U)\defined\coker \shf(U)\xrightarrow{\pphi_U}\shg(U),~\im(\pphi)(U)\defined \im \shf(U)\xrightarrow{\pphi_U}\shg(U)
  \]
  These are easily verified to be presheaves.
\item The presheaf-kernel is in fact already a sheaf, the other two in general are not sheaves however. We define the \emph{sheaf-cokernel} and \emph{sheaf-kernel} as the sheafification of the repsective presheaf.
\end{enumerate}
\end{construction}
\begin{lem}
  Let $\pphi\mc\shf\to\shg$ be a morphism of sheaves.
\begin{enumerate}
  \item The following are equivalent:
  \begin{enumerate}
    \item The morphism $\pphi$ is a monomorphism.
    \item The sheaf-kernel $\ker\pphi$ is zero.
    \item For all points $x\in X$ the induced morphism on stalks $\pphi_x\mc \shf_x\to\shg_x$ is injective.
    \item For all $U\sse X$ open the morphism $\pphi_U$ is injective.
  \end{enumerate}
  \item The following are equivalent:
  \begin{enumerate}
    \item The morphism $\pphi$ is an epimorphism.
    \item The sheaf-cokernel $\coker\pphi$ is zero.
    \item For all points $x\in X$ the induced morphism on stalks $\pphi_x\mc \shf_x\to\shg_x$ is surjective.
  \end{enumerate}
  If all morphisms $\pphi_U\mc \shf(U)\to\shg(U)$ are surjective, then $\pphi$ is an epimorphism. The converse is not true in general.
  \end{enumerate}
\end{lem}
\begin{cor}
  Let $\pphi\mc \shf\to\shg$ be a morphism of sheaves. Then $\pphi$ is an isomorphism if and only if all morphisms on stalks $\pphi_x\mc\shf_x\to\shg_x$ are isomorphisms.
\end{cor}

\begin{defn}
\leavevmode
\begin{enumerate}
  \item Let $\pphi\mc \shf\to\shg$ and $\psi\mc \shg\to\shh$ be morphisms of sheaves. We say that the sequence
  \[
  \shf\xrightarrow{\pphi}\shg\xrightarrow{\psi}\shh
  \]
  is \emph{exact at $\shg$} if $\im(\pphi)=\ker(\psi)$, where we regard both $\im(\pphi)$ and $\ker(\psi)$ as subsheaves of $\shg$.
  \item A short-exact sequence is a sequence of the form
  \[
  0\to\shf\xrightarrow{\pphi}\shg\xrightarrow{\psi}\shh\to 0
  \]
  that is exact at all places.
\end{enumerate}
\end{defn}

\begin{lem}
  \leavevmode
  \begin{enumerate}
    \item A sequence
    \[
    \shf\xrightarrow{\pphi}\shg\xrightarrow{\psi}\shh
    \]
    is exact if and only if all induced sequences of stalks
    \[\shf_x\xrightarrow{\pphi_x}\shg_x\xrightarrow{\psi_x}\shh_x\]
    are exact sequences in the abelian category $\ccat$.
    \item
    A sequence
    \[
  0\to\shf\xrightarrow{\pphi}\shg\xrightarrow{\psi}\shh\to 0
  \]
  is short-exact if and only if all induced sequences of stalks
  \[
  0\to\shf_x\xrightarrow{\pphi_x}\shg_x\xrightarrow{\psi_x}\shh_x\to 0
  \]
  are short-exact sequences of abelian groups.
  \end{enumerate}
\end{lem}

\begin{prop}
  If $\ccat$ is an abelian category that has all limits and colimits, then the category of $\ccat$-valued sheaves is abelian too.
\end{prop}

\begin{construction}
  Let $f\mc X\to Y$ be a continous map of topological spaces.
  \begin{enumerate}
    \item We define the functor $f_{\ast}\mc \asheaf(X)\to\asheaf(Y)$ to map a sheaf $\shf$ on $X$ to the assignment
    \[
    f_{\ast}\shf(U)\defined \shf(f^{-1}U);
    \]
    for every inclusion of open sets $V\sse U$ the restriction map $\rho_{UV}$ for $f_{\ast}\shf$  is given by the restriction $\rho_{f^{-1}(U)f^{-1}(V)}$ of $\shf$:
    \[
    \begin{tikzcd}
      f_{\ast}\shf(U)
      \ar[dashed]{d}
      \ar[equal]{r}
      &
      \shf(f^{-1}U)
      \ar{d}[right]{\rho_{f^{-1}(U)f^{-1}(V)}}
      \\
      f_{\ast}\shf(V)
      \ar[equal]{r}
      &
        \shf(f^{-1}V)
    \end{tikzcd}
    \]
    This can be verified to be a well-defined functor.
    \item The functor $f^{-1}\mc  \asheaf(Y)\to \asheaf(X)$ takes the sheaf $\shg$ on $Y$ to the sheafification of the presheaf
    \[
    U \mapsto
    \lim_{f(U)\sse V \mathrm{~open}}G(V);
    \]
    and restriction maps to the sheafification of the induced maps on colimits.
  \end{enumerate}
\end{construction}
\begin{lem}
  Let $f\mc X\to Y$ be a continous map, $\shf$ a sheaf on $X$ and $\shg$ a sheaf on $Y$.
  \begin{enumerate}
    \item For all points $y\in Y$, we have
    \[
    \left(f_{\ast}\right)_y \cong \lim_{y\in U\sse Y\mathrm{~open}}\shf(f^{-1}(U)).
    \]
  \item For all $x\in X$, we have
  \[
  \left(f^{-1}\shg\right)_x\cong \shg_{f(x)}.
  \]
  \end{enumerate}
\end{lem}

\begin{prop}
  The two functors introduced above form an adjunction
  \[
  f^{-1}\mc \asheaf(Y)\leftrightarrows\asheaf(X)\mcc f_{\ast}
  \]
  in which $f^{-1}$ is the left-adjoint and $f_{\ast}$ is the right-adjoint.
\end{prop}

\begin{cor}
\leavevmode
\begin{enumerate}
  \item
  The functor $f^{-1}\mc \asheaf(Y)\to\asheaf(X)$ is an exact functor.
  \item The functor $f_{\ast}\mc \asheaf(X)\to \asheaf(Y)$ is a left-exact functor, but not exact in general.
\end{enumerate}
\end{cor}

\begin{example}
  If $Y$ is just a point and $f\mc X\to \lset \ast\rset$ is continous, then $f_{\ast}$ is the same as the global sections functor, since we can identify $\asheaf(\lset \ast\rset) = \abc$.
\end{example}

\begin{mexample}\label{3:skyscraper}
  Let $x\in X$ be a single point and $A$ an abelian group. We think of $A$ as a sheaf on the topological space $\lset x\rset$. Then the inclusion $j_x\mc \lset x\rset \hookrightarrow X$ induces a functor $j_{x,\ast}\mc \abc\cong \asheaf(\lset x\rset) \to \asheaf(X)$, and hence $j_{x,\ast}A$ is a sheaf on $X$. Concretely, we have
  \[
  j_{x,\ast}(x') = \begin{cases}
    A&\text{if }x'\in \overline{\lset x\rset}\\
    0&\text{if }x'\notin \overline{\lset x\rset}
  \end{cases}
  \]
  Such a sheaf is called a \emph{skyscraper-sheaf}.
\end{mexample}

\begin{mlem}\label{3:skyscraper-adjunction}
  Let $X$ be a topological space and $x\in X$ a point. Then the functors $(-)_x$ of taking stalks and building the skyscraper $j_{x,\ast}$ are part of an adjunction:
  \[
  j_{x,\ast}\mc \asheaf(\lset x\rset)\leftrightarrows\asheaf(X)\mcc (-)_x
  \]
\end{mlem}

kkk
