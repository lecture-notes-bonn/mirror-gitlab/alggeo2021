\begin{construction}
  We want to compare the group of Weyl divisors with the Picard group of a scheme $X$ that satisfies the assumptions $(\#)$ of \cref{20:divcond}. We define the map
  \[
  K(X)\units \to \wdiv(X),~f\mapsto \sum \nu_Y(f)Y
  \]
  which is a well-defined group homomorphism. An element of such a form of the group of Weyl divisors
  \[
  (f) = \sum \nu_Y(f)Y
  \]
  is called a \emph{principal divisor}. We define the \emph{Class group of $X$} as the quotient of the Weyl group by the principal divisors, i.e. $\cl(X) = \wdiv(X)/\lset (f)\rset$.
\end{construction}

\begin{prop}
 If $X$ is a scheme that satisfies $(\#)$, $Z\ssne X$ a closed subscheme and $U\defined X\setminus Z$ then there is a surjective group homomorphism
  \[
  \cl(X)\twoheadrightarrow \cl(X),~D\mapsto D\cap U
  \]
  which is an isomorphism if $\codim Z\geq 2$.
\end{prop}

  \begin{construction}
   Assume now that $X$ is any scheme and $\emptyset \neq U = \spec A \sse X$ a non-empty affine open subset. Let $S\sse A$ be the set of all non zero-divisors and set $K(U)\defined \sloc A$. The assignement $U\mapsto K(U)$ is a presheaf on $X$, which we sheafify to obtain the sheaf $\kappa_X$, which contains its sheaf of units $\kappa_X\units$.
\end{construction}
\begin{example}
  If $X$ is an integral scheme, then $K(U)=K(X)$ for all open affines $U\sse X$. So $\kappa_X$ is the constant sheaf on $X$.
\end{example}
\begin{construction}
  We have an injection $\ox\units\hookrightarrow\kappa_X\units$, so a short exact sequence of the form
  \begin{equation}\label{21:cartierses}\tag{$\ast$}
  1
  \to
  \ox\units
  \to
  \kappa_X\units
  \to \kappa_X\units/\ox\units
  \to
  1
  \end{equation}
 A \emph{Cartier divisor $D$} is an element of $\hh^0(X,\kappa_X\units/\ox\units)$, so it can be represented by
  \[
  \lset
  f_i\in \kappa_X\units(U_i)\text{ for an open cover }X=\bigcup U_i\ssp \restrict{\frac{f_i}{f_j}}{U_{ij}}\in \ox\units(U_{ij})
  \rset
  \]
  A Cartier divisor that is in the image of the map $\hh^0(X,\kappa_X\units)\to \hh^0(X,\kappa\units/\ox\units)$ induced from \eqref{21:cartierses} is calle a \emph{principal Cartier divisor}. We say that two Cartier Divisors $D_1,D_2$ are \emph{linearly equivalent} if their difference is principal.
  This is an equivalence relation.
  This gives a group $\cacl\defined \hh^0(X,\kappa_X\units/\ox\units)/\sim$, which is called \emph{dunno}.
 \end{construction}

\begin{numtext}
We now want to relate the three groups $\cl(X)$, $\cacl(X)$ and $\pic(X)$.
\begin{enumerate}
  \item We have a natural injection
  \[
  \cacl(X)\hookrightarrow \pic(X),~D\mapsto \oo(D).
  \]
  Furthermore, \eqref{21:cartierses} gives rise to the long exact sequence:
  \[
  1
  \to
  \hh^0(X,\ox\units)
  \to
  \hh^0(X,\kappa_X\units)
  \to
  \hh^0(X,\kappa_X\units/\ox\units)
  \to
  \hh^1(X,\ox\units)
  \to
  \hh^1(X,\kappa_X\units)
  \to
  \ldots
  \]
  We can now insert all the relations we know into this diagram, for $X$ integral:
  \[
  \begin{tikzcd}[column sep = small]
    \hh^0(X,\kappa_X\units)
    \ar{r}
    \ar[twoheadrightarrow]{rd}
    &
    \hh^0(X,\kappa_X\units/\ox\units)
    \ar{rr}
    \ar[twoheadrightarrow]{rd}
    &
    &
    \hh^1(X,\ox\units)
    \ar{rr}
    \ar{d}[right]{\cong}
    &
    &
    0
    \\
    &
    \lset
    \begin{array}{c}
    \text{principal}\\
    \text{Cartier Divisors}
    \end{array}
    \rset
    \ar[hookrightarrow]{u}
    &
    \cacl(X)
    \ar[hookrightarrow]{r}[below]{\text{(A)}}
    &
    \chh^1(X,\ox\units)\cong \pic(X)
    &
  \end{tikzcd}
  \]
  Making $\text{(A)}$ explicit, we have that a short exact sequence of sheaves
  \[
  0
  \to
  \shf
  \to
  \shg
  \to
  \shh
  \to
  0
  \]
  gives rise to the long-exact sequence
  \[
  0
  \to
  \hh^0(X,\shf)
  \to
  \hh^0(X,\shg)
  \to
  \hh^0(X,\shh)
  \xrightarrow{\delta}
  \chh^1(X,\shf)
  \to
  \chh^1(X,\shf)
  \to
  \ldots
  \]
  where the map $\delta$ is given by
  \[
  \delta(f) \defined \lset \restrict{\left(\restrict{f}{U_i}-\restrict{f}{U_j}\right)}{U_{ij}} \rset
  \]
  for the open cover $X=\bigcup U_i$. More explicitly, for such a cover and trivializations $\restrict{\shl}{U_i}\cong \oo_{U_i}$, we have isomorphisms
  \[
  \restrict{\left(\shl\tensor_{\ox}\kappa_X\right)}{U_i}\cong \oo_{U_i}\tensor_{\oo_{U_i}}\restrict{\kappa_X}{U_i}\cong \restrict{\kappa_X}{U_i},
  \]
  where the last sheaf is the constant sheaf $\kappa(X)$. We now arrive at maps
  \[
  \shl
  \hookrightarrow
  \shl \tensor_{\ox}\kappa_X
  \cong
  \kappa_X
  \]
  and via the trivializations at
  \[
  \begin{tikzcd}
    \oo_{U_i}
    \ar{r}[above]{\cong}[below]{\pphi_i^{-1}}
    &
    \restrict{\shl}{U_i}
    \ar[hookrightarrow]{r}
    &
    \kappa_X
  \end{tikzcd}
  \]
  \item If $X$ satisfies the condition $(\#)$, then we a chosen open cover $X = \bigcup U_i$ gives rise to a map
  \begin{align*}
    \cacl(X)&\to \cl(X)
    D\cong \lset f_i \in \hh^0(U_i,\kappa_X^\ast)= \kappa(X)^\ast\rset
    &\mapsto
    \sum_{U_i\cap Y\neq \emptyset}\kappa_Y(f_i)Y
  \end{align*}
\end{enumerate}
So far, we thus arrive at the following commutative diagram for an integral scheme $X$
\[
\begin{tikzcd}
  K\units
  \ar[equal]{r}
  \ar{d}
  &
  K(X)\units = \hh^0(X,\kappa_X\units)
  \ar{d}
  \\
  \wdiv(X)
  \ar[twoheadrightarrow]{d}
  &
  \hh^0(X,\kappa_X\units/ \ox\units)
  \ar[twoheadrightarrow]{d}
  \ar{l}
  \\
  \cl(X)
  &
  \cacl(X)
  \ar{l}
\end{tikzcd}
\]
\end{numtext}
\begin{prop}
  If $X$ is \emph{factorial}, i.e. all stalks $\oxx$ are factorial rings, this map determines an isomorphism $\cacl(X)\to\cl(X)$.
\end{prop}
\begin{lem}
  If $A$ is a factorial ring, then $\cl(A)\defined \cl(\spec A) = 0$.
\end{lem}

\begin{prop}
  Let $X=\spec A$ be affine, with $A$ a Noetherian domain.
  Then the following are equivalent:
  \begin{enumerate}
    \item The ring $A$ is factorial.
    \item The ring $A$ is normal and $\cl(X) = 0$.
  \end{enumerate}
\end{prop}

\begin{example}
  Let $k$ be a field. Then
  \[
  \cl(\pp_n^k)\cong \cacl(\pp_n^k)\cong \pic(\pp_n^k)
  \]
  with $\cacl(\pp_k^n)$ generated by $V_{+}(X_0)$ and $\pic(\pp^n_k)$ generated by $\oo(1)$.  
\end{example}
