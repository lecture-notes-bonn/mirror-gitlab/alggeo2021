\subsection*{Categories with enough injectives}
\begin{prop}
  \leavevmode
  \begin{enumerate}
    \item The category of abelian groups and, more generally, the category of modules over a commutative ring $A$ has enough injectives.
    The functor $\hhom(G,-)\mc\abc\to\abc$ is left-exact. Its higher derived functors are denoted by $\ext^i(G,-)$.
    \item Let $X$ be a topological space. Then the category $\asheaf(X)$ of sheaves on $X$ has enough injectives: First of all, we choose for every $x$ an injective map $\shf_x\hookrightarrow I_x$ in the category of abelian groups. Define the sheaf $I$ as
    \[
    I\defined \prod_{x\in X}j_{x,\ast}\left(I_x\right)
    \]
    where $j_{x,\ast}I_x$ is the skyscraper-sheaf at $x$. Then $I$ is an injective sheaf. For the proof, use \cref{3:skyscraper-adjunction}. As a consequence, we can form the higher derived functors of the global sections functor $\Gamma(X,-)$. These are usually denoted by $\hh^i(X,\shf)\defined \rh^i\Gamma(X,\shf)$ and refered to as \emph{sheaf cohomology}.
    \item More generally, if $f\mc X\to Y$ is a continous map, then $f_{\ast}\mc \asheaf(X)\to \asheaf(Y)$ is left-exact, and hence has higher derived functors
    \[
    \rh^if_{\ast}\mc \shf(X)\to\shf(Y).
    \]
    For a sheaf $\shf$ on $X$ we say that the $\rh^if_{\ast}\shf$ are the \emph{higher direct images} of $\shf$. If $Y=\lset \ast\rset$ is just a point, then the higher direct images of $\shf$ give precisely the sheaf cohomology of $\shf$.
  \end{enumerate}
\end{prop}

\begin{prop}
  Let $f\mc X\to Y$ be a continous map and $\shf$ a sheaf on $X$. Then $\rh^if_{\ast}\shf$ is isomorphic to the sheafification of the presheaf
  \[
  U \mapsto \hh^i\left(f^{-1}(U),\restrict{\shf}{f^{-1}(U)}\right)
  \]
\end{prop}

\begin{defn}
  A sheaf $\shf$ on $X$ is called \emph{flasque} if for all open subsets $U\sse X$ the restriction map $\shf(X)\to\shf(U)$ is surjective.
\end{defn}
\begin{rem}
  \leavevmode
  \begin{enumerate}
  \item If $\shf$ is flasque, then for all open subsets $V\sse U$ the restriction map $\shf(U)\to\shf(V)$ is surjective, and in particular, the sheaf $\restrict{\shf}{U}$ is flasque too.
  \item If $\shf$ is injective, then $\shf$ is flasque too.
  \item If $f\mc X\to Y$ is continous and $\shf$ a flasque sheaf on $X$, then $f_{\ast}\shf$ is a flasque sheaf on $Y$.
  \item If
  \[
  0\to \shf_1\to\shf_2\to\shf_3\to 0
  \]
  is a short-exact sequence of sheaves with $\shf_1$ flasque, then the induced section on global sections
  \[
  0\to \shf_1(X)\to\shf_2(X)\to\shf_3(X)\to 0
  \]
  is short-exact too.
  \item If
  \[
  0\to \shf_1\to\shf_2\to\shf_3\to 0
  \]
  is again a short-exact sequence of sheaves with $\shf_1,\shf_2$ flasque, then $\shf_3$ is flasque too.
  \item If $\shf$ is flasque then $\shf$ is $\Gamma$-acylic. In particular, we can use flasque sheaves to compute sheaf cohomology.
  \item More generally, if $f\mc X\to Y$ is continous, and $\shf$ is a flasque sheaf on $X$, then $\shf$ is $f_{\ast}$-acyclic.
  \end{enumerate}
\end{rem}

\subsection*{Explicit computations using \v{C}ech cohomology}
\begin{construction}
  Let $X$ be a topological space and $X = \bigcup_{i\in I}U_I$ and open cover with $I$ a well-ordered index set. Let $\shf$ be a sheaf on $X$. For elements $i_0<\ldots<i_p$ of $I$, set
  \[
  U_{i_0\ldots i_p}\defined U_{i_0}\cap \ldots \cap U_{i_p}.
  \]
  Set now
  \[
  \ch^0(\shf)\defined \prod_{i\in I}\shf(U_i)
  \]
  and, more generally, for $p\geq 0$
  \[
  \ch^p(\shf)\defined \prod_{i_0<\ldots<i_p}\shf\left(U_{i_0\ldots i_p}\right).
  \]
  We then have natural maps
  \[
  \shf(X)\to \ch^0(\shf),~s\mapsto\left(\restrict{s}{U_i}\right)_{i\in I},
  \]
  as well as
  \[
  \ch^0(\shf)\xrightarrow{d_0}\ch^1(\shf),~\left(s_i\right)_{i\in I}\mapsto \left( \restrict{s_{i_1}}{U_{i_0i_1}}- \restrict{s_{i_0}}{U_{i_0i_1}}\right)
  \]
  and for a general $p$
  \[
  \ch^p(\shf)\xrightarrow{d_p}\ch^{p+1},~\left(s_{i_0\ldots i_p}\right)_{i_0<\ldots i_p} \mapsto
  \left(
  \sum_{k=0}^{p+1}\left(-1\right)^k \restrict{s_{i_0\ldots \hat{i}_k\ldots i_{p+1}}}{U_{i_0\ldots i_{p+1}}}
  \right)_{i_0<\ldots<i_{p+1}}
  \]
  Then these abelian groups form a chain complex of the form
  \[
  0\to \shf(X)\to \ch^0(\shf)\xrightarrow{d^0}\ch^1(\shf)\xrightarrow{d^1}\ldots.
  \]
  For such an open covering $\lset U_i\rset$, we define the \emph{\v{C}ech cohomology of $X$} $\chh^i\left(\lset U_i\rset, \shf\right)$ to be the $i$-th cohomology of the chain complex:
  \[
  \ch^0(\shf)\xrightarrow{d^0}\ch^1(\shf)\xrightarrow{d^1}\ldots
  \]
\end{construction}
