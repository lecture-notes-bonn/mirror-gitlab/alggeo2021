\subsection*{Applications of the Fibre Product}
\begin{defn}
  Let $f\mc X\to Y$ be a morphism of schemes and $y\in Y$. Then the \emph{scheme theoretic fibre $X_y$ of $f$ over $y$} is given by the fibre product
  \[
  \begin{tikzcd}
    \spec (\kappa(y))\fip{Y}X
    \ar{r}
    \ar{d}
    \fipsym{rd}
    &
    X
    \ar{d}[right]{f}
    \\
    \spec(\kappa(y))
    \ar{r}
    &
    Y
  \end{tikzcd},\]
  where $\spec \kappa (y)\to Y$ denotes the map from \todo{exercise 17}.
\end{defn}
\begin{prop}
  In the notation as above, the projection $X_y\to X$ induces a homeomorphism $\abs{X_y}\cong f^{-1}(y)$.
\end{prop}
\begin{proof}
  On topological spaces, the map $X_y\to X$ factors over $f^{-1}(y)$, since the diagram
  \[
  \begin{tikzcd}
    \abs{X_y}
    \ar{r}[above]{\abs{p}}\ar{d}
    &
    \abs{X}
    \ar{d}[right]{\abs{f}}
    \\
    \lset \ast\rset
    \ar{r}[below]{\ast\mapsto y}
    &
    \abs{Y}
  \end{tikzcd}
  \]
  commutes. We show that the induced map is indeed a homeomorphism:
  \begin{itemize}
    \item The map is surjective: Let $x\in f^{-1}(y)$. We already have the natural morphism $\spec \kappa(x)\to X$ corresponding to $x$. We also have a morphism $\spec \kappa(x)\to \spec \kappa(y)$, which is induced by the following commutative square
    \[
    \begin{tikzcd}
      \oo_{Y,y}
      \ar{r}
      \ar{d}
      &
      \oxx
      \ar{d}
      \\
      \kappa(y)
      \ar[dashed]{r}
      &
      \kappa(x)
    \end{tikzcd}
    \]
    So we get an induced map $\spec(\kappa(x))\to X_y$, which gives the desired element in the preimage:
    \[
    \begin{tikzcd}
      \spec(\kappa(x))
      \ar[bend right = 30]{rdd}
      \ar[bend left = 30]{drr}
      \ar[dashed]{dr}
      &
      &
      &
      \\
      &
      X_y
      \ar{r}[above]{q}\ar{d}\fipsym{dr}&
      X\ar{d}[right]{f}\\
      &
      \spec(\kappa(y))\ar{r}
      &
      Y
    \end{tikzcd}
    \]
    \item The map is injective: A set-theoretic argument would go along the following lines: Let $x_1,x_2\in X_y$ be two points that map to the same $x\in f^{-1}(y)$. Then they induce maps $\pphi_i\mc \lset\ast\rset\to X_y$ that make the diagram
    \[
    \begin{tikzcd}
      \lset\ast\rset
      \ar[bend right = 30]{rdd}
      \ar[bend left = 30]{drr}[above right]{\ast\mapsto x}
      \ar[dashed, shift left = 2pt]{dr}[above right]{\pphi_1}
      \ar[dashed, shift right = 2pt]{dr}[below left]{\pphi_2}
      &
      &
      &
      \\
      &
      X_y
      \ar{r}[above]{q}\ar{d}\fipsym{dr}&
      X\ar{d}[right]{f}\\
      &
      \spec(\kappa(y))\ar{r}
      &
      Y
    \end{tikzcd}
    \]
    commute. Then the universal property of $X_y$ implies $\pphi_1=\pphi_2$ and thus $x_1=x_2$. The problem is that there is no canonical scheme structure on $\lset\ast\rset$ to make this into a scheme-theoretic argument. \par
    So we need to think differently -- consider first the affine case $X=\spec B$, $Y=\spec A$. Then $y\in Y$ corresponds to a prime ideal $\idp\sse A$ and the scheme-theoretic fibre is given by
    \begin{align*}
    X_y &= \spec\left(A_{\idp}/\idp A_{\idp}\tensor_A B\right)\\
        &= \spec\left(S^{-1}B/(\idp S^{-1}B)\right),
    \end{align*}
    where $S$ is the image of the map $A\setminus \idp\to B$. Using the explicit description of the spectrum of localizations and quotients, we get
    \begin{align*}
      X_y &=
      \lset \idq\sse B\text{ prime}\ssp
      \begin{aligned}
        &\idq\cap S = \emptyset,\text{ i.e. }\idq^c\sse \idp \\
        &\idq\supseteq \idp S^{-1}B,\text{ i.e. } \idq^c\supseteq \idp
      \end{aligned}
      \rset \\
      &= \lset \idq\sse B\text{ prime}\ssp \idq^c=\idp\rset = f^{-1}(y).
    \end{align*}
  \end{itemize}
\end{proof}
