\section{Sheaves}
Let $X$ be a topological space.
\begin{defn}
  Let $\ccat$ be a category.
  A $\ccat$-\emph{valued}\toidx{presheaf} \emph{on $X$} consists of the following data:
  \begin{enumerate}
    \item for every open subset $U\sse X$ an object $\shf(U)$ in $\ccat$;
    \item for every inclusion $V\sse V$ of open sets a morphism $\rho_{UV}\mc \shf(U)\to \shf(V)$
  \end{enumerate}
  such that
  \begin{enumerate}
    \item[PS1)] the morphism $\rho_{UU}\mc \shf(U)\to \shf(U)$ is the identity;
    \item[PS2)] for all inclusion $W\sse V\sse U$ of open sets the diagram
    \[
    \begin{tikzcd}
      (U)\ar{r}[above]{\rho_{UV}}\ar[bend right= 20]{rr}[below]{\rho_{UW}}&\shf(V)\ar{r}[above]{\rho_{VW}}&\shf(W)
    \end{tikzcd}
    \]
    commutes.
  \end{enumerate}
\end{defn}
\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item In this lecture, we will mostly consider presheaves of abelian groups.
    \item Some people also add the requirement that $\shf(\emptyset)$ is the terminal object in $\ccat$.
  \end{enumerate}
\end{rem}

\begin{notation}
  Let $\shf$ be a $\ccat$-valued presheaf on $X$ such that $\ccat$ admits a forgetful functor to the category of sets, and $U\sse X$ an open subset.
  \begin{enumerate}
    \item Elements $s\in \shf(U)$ are called \emph{sections of $\shf$ over $U$}.\index{section}
    \item The object $\Gamma(U,F)$ is called \emph{the space of sections over $U$}.
    \item For $U=X$, $\Gamma(U,X)$ is called the \emph{space of global sections}.
    \item For $s\in \shf(U)$ and $V\sse U$ an open subset, the element $\restrict{s}{V}\defined \rho_{UV}(s)$ is called the \emph{restriction of $s$ to $V$}.
  \end{enumerate}
\end{notation}


\begin{example}
  \leavevmode
  \begin{enumerate}
    \item For $X$ any topological space, we denote by $C$ the presheaf of continous functions $X\to \rr$, where the restriction are given by actual restrictions of maps.
    \item More generally, for $X$ and $Y$ topological spaces we can consider the presheaf $\shf$ which associates to every open subset $U\sse X$ the set of continous functions $U\to Y$.
  \end{enumerate}
\end{example}


\begin{construction}[Presheaves as functors]
  For a topological space $X$ we can consider the category $\ouv(X)$, where objects are given by open subsets of $X$ and morphisms correspond to inclusions. Then a $\ccat$-valued presheaf $\shf$ on $X$ is just a functor $\op{\ouv(X)}\to \ccat$.
\end{construction}

\begin{example}
  Let $A$ be a ring. Consider $X\defined \spec(A)$ which is the set of all prime ideals in $A$ endowed with the \emph{Zariski topology}: the closed sets in $X$ are given by sets of the form
  \[
  V(\aid)\defined \lset \pid~\text{prime ideal in }A\ssp \aid\sse \pid\rset
  \]
  for all ideals $\aid\sse A$;
  a basis of open sets is given by the sets of the form
  \[
  D(a)\defined \lset \pid~\text{prime ideal in }A\ssp a\notin \pid\rset
  \]
  for all elements $a\in A$.
  Then the \emph{structure (pre)-sheaf} $\ox$ of rings is given on an open subset $U\sse X$ by the set of all maps
  \[
  s\mc U\to \coprod_{\pid\in U}A_{\pid}
  \]
  such that for all prime ideals $\pid \in U$ it holds that
  \begin{enumerate}
    \item $s(\pid)\in A_{\pid}$;
    \item there exists an open neighbourhood $\pid\in V\sse U$ and elements $a,b\in A$ such that for all $\qid\in V$ we have $b\notin \qid$ and $s(\qid)=a/b\in A_{\qid}$.
  \end{enumerate}
  For an open subset $U\sse X$, the ring structure on $\ox(U)$ is given by pointwise addition and multiplication:
  for two maps $s_1,s_2\mc U \to \coprod A_\pid$ and a prime ideal $\qid\in U$, both $s_1(\qid)$ and $s_2(\qid)$ are elements of $A_\qid$, by i), so $s_1(\qid)\cdot s_2(\qid)$ and $s_1(\qid)+ s_2(\qid)$ are well defined operations in $A_{\qid}$.\todo{why does this satisfy ii)?}\par
  For an inclusion of open subsets $V\sse U$, the restriction maps are given by the restrictions of the maps $s$ to $V$ (note that this well-defined as $\coprod_{\pid \in V}A_{\pid} \sse \coprod_{\pid \in U}A_{\pid}$).
\end{example}

\begin{rem}
  Let $\kf$ be an algebraically closed field and $A$ a $k$-algebra of finite type. For $U\in \maxspec(A)$ a map $s\in \ox(U)$ gives a map $\overline{s}\mc U\to k$, which maps $\pid\in U$ to $\overline{s(\pid)}\in A/\pid A_{\pid}\cong \kf$.
\end{rem}

\begin{defn}
  Let $\shf_1,\shf_2$ be two $\ccat$-valued presheaves on a topological space $X$. A \emph{morphism of presheaves} $\eta\mc \shf_1\to \shf_2$ is a natural transformation of the functors $\shf_1,\shf_2\mc \op{\ouv(X)}\to \ccat$.\index{presheaf!morphism of}
  Spelled out, this correspond to a collection of morphisms $\eta_U\mc \shf_1(U)\to \shf_2(U)$ in $\ccat$ for every open subset $U\sse X$, such that for every inclusion of open subset $V\sse U$ the diagram
  \[
  \begin{tikzcd}
  \shf_1(U)\ar{r}[above]{\eta_U}\ar{d}[left]{\rho^{\shf_1}_{UV}}&
  \shf_2(U)\ar{d}[right]{\rho^{\shf_2}_{UV}}\\
  \shf_1(V)\ar{r}[below]{\eta_V}&\shf_2(V)
  \end{tikzcd}
  \]
  commutes.
  This gives the category $\psc{C}(X)$ of $\ccat$-valued presheaves on $X$, which is nothing but the functor category of functors $\op{\ouv(X)}\to \ccat$.
\end{defn}

\begin{rem}
  A morphism of presheaves $\eta\mc\shf_1\to\shf_2$ is an isomorphism of presheaves in the usual if and only if the morphisms $\eta_U\mc \shf_1(U)\to\shf_2(U)$ are isomorphisms in $\ccat$ for all open subsets $U\sse X$.
\end{rem}

\begin{example}
  Let $X$ be a differential manifold, and $C$ the presheaf of continous functions on $X$ and $C^{\text{diff}}$ the presheaf of differential functions. Then the inclusion $C^{\text{diff}}\hookrightarrow C$ is a morphism of presheaves.
\end{example}

\begin{defn}
  Let $\shf$ be $\ccat$-valued presheaf on $X$. Then $\shf$ is called a \toidx{sheaf} if for all open subsets $U\sse X$ and open coverings $U = \bigcup_i U_i$ the object $\shf(U)$ is an equalizer of
  \[
  \prod_i \shf(U_i)  \rightrightarrows
  \prod_{i,j} \shf(U_i\cap U_j),
  \]
  where the upper arrow maps a collection $\left(s_i\right)$ to the tupel $\left(\restrict{s_i}{U_i\cap U_j}\right)_{ij}$ and the lower arrow maps a collection $\left(s_j\right)$ to the tupel $\left(\restrict{s_i}{U_i\cap U_j}\right)_{ij}$. Spelled out, this means that if $\left(s_i\in \shf(U_i)\right)_i$ is a collection of sections in the $\shf(U_i)$ such that
  \[
  \restrict{s_i}{U_i\cap U_j} =
  \restrict{s_j}{U_i\cap U_j}
  \]
  holds for all $i,j\in I$ then there is a unique section $s\in U$ such that $\restrict{s}{U_i} = s_i$. A \emph{morphism of sheaves} is a morphism of presheaves between sheaves. In particular, we obtain the category $\sheaf{C}$ of $\ccat$-valued sheaves as the full subcategory of $\psc{C}$ consisting of all sheaves. We denote by $\asheaf$ the category of sheaves with values in abelian groups.
\end{defn}

\begin{example}
  \leavevmode
  \begin{enumerate}
    \item For $X=\spec(A)$, the structure sheaf $\ox$ is indeed a sheaf:

    \item Let $G$ be an abelian group.
  \end{enumerate}
\end{example}

\begin{recall}
  A \emph{partially ordered set} is a set $I$ together with a binary relation $\leq$ such that the following three conditions are satisfied:
  \begin{enumerate}
    \item For all $i\in I$ it holds that $i\leq i$;
    \item If $i\leq j$ and $j\leq k$ then $i\leq k$ holds;
    \item If $i\leq j$ and $j\leq i$ then $i=j$ holds.
  \end{enumerate}
  A partially ordered set is called \emph{directed} if for all $i,j\in I$ there is a $k\in I$ such that $i,j\leq k$.
\end{recall}
\begin{example}\label{1:directsystemexample}
\leavevmode
\begin{enumerate}
  \item Every totally ordered set (e.g. the set of natural numbers) is a directed partially ordered set.
  \item Let $X$ be a topological space and $S\sse X$ any subset. Then the set of open subsets containing $S$ is directed, by setting $U\leq V$ if $V\sse U$. For example, $S$ could be a point $\ast$ (so we are considering all open subsets containing $\ast$) or the empty set (then we are considering all open subsets of $X$).
  \end{enumerate}
\end{example}

\begin{rem}
  Let $I$ be a partially ordered set. This gives rise to a category $\icat$ with objects given by $I$ and morphisms given by:
  \[
  \mor_{\icat}(i,j) = \begin{cases}
    \lset (i,j)\rset & \text{if } j\leq i\\
    \emptyset & else
  \end{cases}
  \]

\end{rem}

\begin{defn}
\leavevmode
\begin{enumerate}
  \item Let $I$ be a directed set and $\ccat$ a category. A \emph{directed system of objects in $\ccat$} is a functor $\op{\icat}\to \ccat$. Spelled out, this corresponds to the following:
  \begin{itemize}
    \item A collection $\left(A_i\right)_{i\in I}$ of objects in $\ccat$ index by $I$;
    \item For every $i\leq j$ a morphism $\rho_{ij}\mc A_i\to A_j$ in $\ccat$;
  \end{itemize}
  such that the following holds:
  \begin{enumerate}
    \item For every $i\in I$, we have $\rho_{ii} = \id_{A_i}$;
    \item For every $i\leq j\leq k\in I$, the diagram
    \[
    \begin{tikzcd}
      A_i\ar{r}[above]{\rho_{ij}}\ar[bend right = 20]{rr}[below]{\rho_{ik}}&A_j\ar{r}[above]{\rho_{jk}}&A_k
    \end{tikzcd}
    \]
    commutes.
  \end{enumerate}
  \item A \emph{direct limit or colimit of a directed system} of a directed system $(A_i,\rho_{ij})$ is given by an object $\lim A_i$ and morphisms $f_i\mc A_i\to \lim A_i$ such that $f_j\circ \rho_{ij} = f_i$ such that $\lim A_i$ is initial with this property:
  If $A$ is another object in $\ccat$ and $F_i\mc A_i\to A$ a collection of morphisms such that $F_j\circ \rho_{ij}  = F_i$ then there is a unique morphism $F\mc \lim A_i\to A $ such that the whole diagram
  \[
  \begin{tikzcd}
    A_i\ar{rr}[above]{\rho_{ij}}\ar{rd}[below left]{f_i}
    \ar[bend right = 20]{rdd}[below left]{F_i}
    &
    &
    A_j\ar{ld}[below right]{f_j}
    \ar[bend left = 20]{ldd}[below right]{F_j}
    \\
    &
    \lim A_i \ar[dashed]{d}[left]{\exists !F}&
    \\
    &
    A
    &
  \end{tikzcd}
  \]
  commutes.
  \end{enumerate}
\end{defn}

\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item Direct limits do not exist in general. If they do, they are unique up to unique isomorphism.
  \end{enumerate}
\end{rem}
